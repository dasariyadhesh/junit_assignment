package com.innominds.assignments.unittest;

public class Q14Test {
	public static void main(String[] args) {
		Q14EmployeeDB empDB = new Q14EmployeeDB();
		Q14Employee emp1 = new Q14Employee(1, "Yadhesh", "yadhesh@gmail.com", 'M', 250000);
		Q14Employee emp2 = new Q14Employee(2, "Srikanth", "Srikanth@gmail.com", 'M', 350000);
		Q14Employee emp3 = new Q14Employee(3, "Ashok", "Ashhok@gmail.com", 'M', 450000);
		Q14Employee emp4 = new Q14Employee(4, "Divya", "Divya@gmail.com", 'F', 550000);
		empDB.addEmployee(emp1);
		empDB.addEmployee(emp2);
		empDB.addEmployee(emp3);
		empDB.addEmployee(emp4);
		for (Q14Employee emp : empDB.listAll())
			System.out.println(emp.getEmpDetails());
		System.out.println();
		
		empDB.deleteEmployeeID(2);

		for (Q14Employee emp : empDB.listAll())
			System.out.println(emp.getEmpDetails());

		System.out.println();
		System.out.println(empDB.showPaySlip(1));

	}
}
