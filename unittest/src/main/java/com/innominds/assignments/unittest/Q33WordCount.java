package com.innominds.assignments.unittest;

public class Q33WordCount {
	public int wordCount(String str) {
		int count = 0;
		for (int i = 0; i <= (str.length() - 1); i++) {
			if (((i > 0) && (str.charAt(i) != ' ') && (str.charAt(i - 1) == ' '))
					|| ((str.charAt(i) != ' ') && (i == 0)))
				count++;
		}
		return count;
	}
	/*
	public static void main(String arg[]) {
		Q33WordCount obj = new Q33WordCount();
		System.out.print("word count : ");
		System.out.println(obj.wordCount(" Hi this is Yadhesh"));
	}
	*/

}
