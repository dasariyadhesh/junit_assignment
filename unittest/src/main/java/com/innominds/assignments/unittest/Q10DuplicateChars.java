package com.innominds.assignments.unittest;

import java.util.ArrayList;

public class Q10DuplicateChars {
	public ArrayList<Character> duplicateCharacters(String str) {
		char[] arr = str.toCharArray();
		ArrayList<Character> duplicates = new ArrayList<>();
		for (int i = 0; i < str.length(); i++) {
			for (int j = i + 1; j < str.length(); j++) {
				if (arr[i] == arr[j]) {
					duplicates.add(arr[j]);
				}
			}
		}
		return duplicates;

	}
	/*public static void main(String[] args) {
		Q10DuplicateChars obj=new Q10DuplicateChars();
		System.out.println(obj.duplicateCharacters("aabbccdd"));
	}
	*/
}
