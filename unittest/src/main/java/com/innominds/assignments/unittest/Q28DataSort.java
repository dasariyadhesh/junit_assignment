package com.innominds.assignments.unittest;

public class Q28DataSort {
	public char[] dataSorting(String str) {
		char temp = 0;
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			for (int j = 0; j < chars.length; j++) {
				if (chars[j] > chars[i]) {
					temp = chars[i];
					chars[i] = chars[j];
					chars[j] = temp;
				}
			}
		}
	return chars;
	}
	public static void main(String[] args) {
		Q28DataSort obj=new Q28DataSort();
		System.out.println(obj.dataSorting("321YADHESHyadhesh"));
	}
}
