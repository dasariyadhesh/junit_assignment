package com.innominds.assignments.unittest;

public class Q9IncomeTaxCalc {
	public double incomeTaXCalc(int income) {
		double tax = 0;
		if (income <= 50000) {
			tax = 0;
		} else if (income > 50000 && income <= 60000) {
			income = income - 50000;
			tax = 0.1 * income;
		} else if (income > 60000 && income <= 150000) {
			income = income - 60000;
			tax = (0.2 * income) + 1000;
		} else {
			income = income - 150000;
			tax = (0.3 *income) + 1000 + 18000;

		}
		return tax;
	}

	/*public static void main(String[] args) {
		Q9IncomeTaxCalc obj = new Q9IncomeTaxCalc();
		System.out.println(obj.incomeTaXCalc(200000));
	}
	*/
	
}
