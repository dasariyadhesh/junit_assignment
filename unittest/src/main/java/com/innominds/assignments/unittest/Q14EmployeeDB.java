package com.innominds.assignments.unittest;

import java.util.ArrayList;

public class Q14EmployeeDB {
	ArrayList<Q14Employee> employee=new ArrayList<>();
	public boolean addEmployee(Q14Employee e) {
		return employee.add(e);
	}
	
		public Q14Employee deleteEmployeeID(int empId){
		     return employee.remove(empId);
		}
	public String showPaySlip(int empId) {
		String paySlip="Invalid employee id";
		for(Q14Employee e:employee) {
			if(e.getEmpId()==empId) {
				paySlip="income for employee id "+empId+" is "+e.getEmpSalary();
			}
			
		}
		return paySlip;
	}
	public Q14Employee[] listAll() {
		Q14Employee[] empArray=new Q14Employee[employee.size()];
		for(int i=0;i<employee.size();i++) {
			empArray[i]=employee.get(i);
		}
		return empArray;
	}
	
}


