package com.innominds.assignments.unittest;

import java.io.BufferedReader;
import java.io.FileReader;

public class Q34CountLines {
	@SuppressWarnings("resource")
	public int countLines(String fileName) {
		BufferedReader br;
		int lineCount = 0;
		try {
			br = new BufferedReader(new FileReader(fileName));
		} catch (Exception e) {
			System.out.println("Error. can't open File");
			return 0;
		}
		try {
			String line = br.readLine();
			while (line != null) {
				lineCount++;
				line = br.readLine();
			}
		} catch (Exception e) {
			System.out.println("Error. problem with reading file");
			return 0;
		}
		return lineCount;
	}
	/*
	public static void main(String[] args) throws IOException {
		Q34CountLines obj = new Q34CountLines();
		if (args.length == 0) {
			System.out.println("Usage:   java LineCounts <file-names>");
			return;
		}

		for (int i = 0; i < args.length; i++) {
			System.out.print(args[i] + ":  ");
			System.out.println(obj.countLines(args[i]));
		}
		
	}
	*/
	
}
