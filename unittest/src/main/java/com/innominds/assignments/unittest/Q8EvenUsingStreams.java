package com.innominds.assignments.unittest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Q8EvenUsingStreams {
	//int[] list= {1,2,3,4,5,6,7,8,9,10};
	public static ArrayList<Integer> evens(int[] list) {
	    return Arrays.stream(list)
	             .boxed()
	             .filter(i -> i % 2 == 0)
	             .collect(Collectors.toCollection(ArrayList::new));
	}
	/*public static void main(String[] args) {
		Q8EvenUsingStreams obj=new Q8EvenUsingStreams();
		System.out.println(obj.evens(obj.list));
	}
	*/
}
