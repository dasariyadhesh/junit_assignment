package com.innominds.assignments.unittest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Q7DateDiff {
	public long dateDiff(String firstDate,String secondDate) {
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate date1=LocalDate.parse(firstDate, formatter);
		LocalDate date2=LocalDate.parse(secondDate, formatter);
		long daysBetween=Math.abs(ChronoUnit.DAYS.between(date1,date2));
		return daysBetween;
		
	}
	/*public static void main(String[] args) {
		Q7DateDiff q7=new Q7DateDiff();
		System.out.println(q7.dateDiff("10/07/2022","11/07/2019"));
	}
	*/
	
}
