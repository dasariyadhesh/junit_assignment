package com.innominds.assignments.unittest;

/*Write unit tests for a program to check the input characters for uppercase,
 *  lowercase, number of digits and other characters 
 */
public class Q2CharsCheck {

	public int checkUpper(String str) {
		int up = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
				up++;
			}
		}
		// System.out.println("Result :"+up);
		return up;
	}

	public int checkLower(String str) {
		int lc = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
				lc++;
			}
		}
		// System.out.println("Result :"+l);
		return lc;
	}

	public int checkDigit(String str) {
		int d = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= '0' && str.charAt(i) <= '9') {
				d++;
			}
		}
		// System.out.println("Result :"+d);
		return d;
	}

	public int checkOthers(String str) {
		int spl = 0;
		for (int i = 0; i < str.length(); i++) {
			if (!(str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') && !(str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
					&& !(str.charAt(i) >= '0' && str.charAt(i) <= '9')) {
				spl++;
			}
		}

		// System.out.println("Result :"+s);
		return spl;
	}
}
