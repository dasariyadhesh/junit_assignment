package com.innominds.assignments.unittest;

import java.util.ArrayList;

public class Q6EvenNum {
	private ArrayList<Integer> A1 = new ArrayList<Integer>();

	public ArrayList<Integer> saveEvenNumbers(int N) {
		if (N < 2) {
			System.out.println("Enter valid positive number greater than euqal to 2");

		} else {
			A1 = new ArrayList<Integer>();
			for (int i = 2; i <= N; i++) {
				if (i % 2 == 0)
					A1.add(i);
			}
		}
		return A1;

	}

	public ArrayList<Integer> printEvenNumbers(ArrayList<Integer> A1) {
		ArrayList<Integer> A2 = new ArrayList<Integer>();
		for (int item : A1) {
			A2.add(item * 2);
		}

		return A2;

	}

}
