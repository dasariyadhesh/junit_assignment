package com.innominds.assignments.unittest;

public class Q11Vehicles {
	public String avoidCollision(String car1Direction, String car2Direction) {
		try {
			if (car1Direction.equals(car2Direction)) {
				return "cars will not Collide";
			} else
				throw new Exception("cars will Collide") ;

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;

	}
	public static void main(String[] args) {
		Q11Vehicles obj = new Q11Vehicles();
		System.out.println(obj.avoidCollision("Left", "Right"));
	}
	
}
