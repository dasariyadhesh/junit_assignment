package com.innominds.assignments.unittest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Q26FileCompressUncompress {

	@SuppressWarnings("unused")
	public void compressFile(String fileToCompress, String compressFile) throws IOException {
		try (FileInputStream fin = new FileInputStream(fileToCompress);
				FileOutputStream fout = new FileOutputStream(compressFile);
				DeflaterOutputStream dos = new DeflaterOutputStream(fout);) {

			int i;
			while ((i = fin.read()) != -1) {
				dos.write((byte) i);
				dos.flush();
			}

		}

		System.out.println("Compression is done..");
	}

	@SuppressWarnings("unused")
	public void decompressFile(String fileToDeCompress, String deCompressFile) throws IOException {
		try (FileInputStream fin = new FileInputStream(fileToDeCompress);
				InflaterInputStream in = new InflaterInputStream(fin);
				FileOutputStream fout = new FileOutputStream(deCompressFile);) {

			int i;
			while ((i = in.read()) != -1) {
				fout.write((byte) i);
				fout.flush();
			}

		}

		System.out.println("DeCompression is done..");

	}
	public static void main(String args[]) throws IOException {
		Q26FileCompressUncompress compressDecompressFileDemo = new Q26FileCompressUncompress();
        compressDecompressFileDemo.compressFile("Q37Answers.txt","compressed.txt");
        compressDecompressFileDemo.decompressFile("compressed.txt","uncompressed.txt");

	}

}