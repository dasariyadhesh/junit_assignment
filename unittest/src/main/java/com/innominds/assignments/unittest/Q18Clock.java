package com.innominds.assignments.unittest;

public class Q18Clock{
	int hours,minutes,seconds;

	public Q18Clock(int hours, int minutes, int seconds) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	public String isTimeValid() {
		if(hours>=0 && hours<24 && minutes>0 && minutes <60 && seconds>0 && seconds <60)
			return "Time is Valid";
		else
			return "Time is Invalid";
	}
	public String setTimeMode() {
		if(hours<12)
			return "Time = "+hours+":"+minutes+":"+seconds+" AM";
		else
			return "Time = "+(hours-12)+":"+minutes+":"+seconds+" PM";
	}
	/*
	public static void main(String[] args) {
		Q18Clock q18Obj=new Q18Clock(13,59,59);
		System.out.println(q18Obj.setTimeMode());
	}
	*/
}

