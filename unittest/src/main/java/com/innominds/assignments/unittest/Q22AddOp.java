package com.innominds.assignments.unittest;

public class Q22AddOp {
	public int add(int num1,int num2) {
		return num1+num2;
		
	}

	@FunctionalInterface
	interface MathOperation {
		int operation(int number1, int number2);
	}
	
	MathOperation additionOp = (number1, number2) -> number1 + number2;
}
