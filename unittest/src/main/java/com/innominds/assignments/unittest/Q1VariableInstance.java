package com.innominds.assignments.unittest;

/*Write unit tests for a program to show that the value of 
 * non-static variable is not visible to all the instances and therefore cannot
 * be used to count the number of instances
 */
public class Q1VariableInstance {
	int nonStaticInstance = 0;
	static int staticInstance = 0;

	public Q1VariableInstance() {
		nonStaticInstance++;
		staticInstance++;
	}

}
