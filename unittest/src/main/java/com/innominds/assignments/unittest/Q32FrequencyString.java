package com.innominds.assignments.unittest;

import java.util.HashMap;

public class Q32FrequencyString {
	public HashMap<Character, Integer> stringFrequency(String str) {
	HashMap<Character,Integer> freq=new HashMap<Character,Integer>();
	str=str.toLowerCase();
	char[] strArr=str.toCharArray();
	for(char c:strArr) {
		if(freq.containsKey(c)) {
			freq.put(c,freq.get(c)+1);
		}
		else {
			freq.put(c,1);
		}
	}
	return freq;
	}
	/*
	public static void main(String[] args) {
		Q32FrequencyString obj= new Q32FrequencyString();
		System.out.println(obj.stringFrequency("Yadhesh"));
	}
	*/
	
}
