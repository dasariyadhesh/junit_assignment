package com.innominds.assignments.unittest;

import java.util.ArrayList;

public class Q17PrimeNum {
	public ArrayList<Integer> primenum(int[] arr) {
		ArrayList<Integer> prime = new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			boolean isPrime = true;
			if (arr[i] == 1)
				isPrime = false;
			else {
				for (int j = 2; j <= arr[i] / 2; j++) {
					if (arr[i] % j == 0) {
						isPrime = false;
						break;
					}
				}
			}
			if (isPrime) {
				prime.add(arr[i]);
			}
		}
		return prime;
	}
	/*
	 * public static void main(String[] args) { Q17PrimeNum obj = new Q17PrimeNum();
	 * int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	 * System.out.println(obj.primenum(arr)); }
	 */
}
