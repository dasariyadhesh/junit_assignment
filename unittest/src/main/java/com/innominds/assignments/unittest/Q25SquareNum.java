package com.innominds.assignments.unittest;

import java.util.Arrays;

public class Q25SquareNum {
	public int[] squreNum(int num) throws Exception {
		int[] square = new int[num];
		try {
			if (num <= 10) {
				for (int i = 0; i < num; i++) {
					square[i] = (int) Math.pow(i + 1, 2);
				}
			} else {
				throw new Exception("Enter number less than are equal to 10");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return square;

	}
	
	public static void main(String[] args) throws Exception {
		Q25SquareNum obj = new Q25SquareNum();
		System.out.println(Arrays.toString(obj.squreNum(4)));
	}
	

}
