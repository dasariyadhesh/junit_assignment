package com.innominds.assignments.unittest;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Q39CurrentDate {
	public String currentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}

	public String add10HoursToDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
		Date oldDate = new Date(System.currentTimeMillis());
		Date newDate = new Date(oldDate.getTime() + TimeUnit.HOURS.toMillis(10));
		return formatter.format(newDate);
	}

	public static void main(String[] args) {
		Q39CurrentDate obj = new Q39CurrentDate();
		System.out.println(obj.currentDate());
		System.out.println(obj.add10HoursToDate());
	}
}
