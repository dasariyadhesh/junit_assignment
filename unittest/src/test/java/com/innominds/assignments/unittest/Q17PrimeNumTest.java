package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class Q17PrimeNumTest {
	Q17PrimeNum q17Obj=new Q17PrimeNum();
	
	@Test
	void testPrimeNumbers() {
		int[] array= {1,2,3,4,5,6,7,9,10};
		Object[] expected= {2,3,5,7};
		assertArrayEquals(expected,q17Obj.primenum(array).toArray());
		
	}
	@Test
	void testNonPrimeNumbers() {
		int[] array= {1,2,3,4,5,6,7,9,10};
		Object[] expected= {1,4,6,8};
		assertNotEquals(expected,q17Obj.primenum(array).toArray());
	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q17Obj.primenum(null);} );
	}

}
