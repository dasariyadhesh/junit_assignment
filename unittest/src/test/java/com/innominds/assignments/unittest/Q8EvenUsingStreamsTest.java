package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class Q8EvenUsingStreamsTest {
	@Test
	void testEvenList() {
		int[] list= {1,2,3,4,5,6,7,8,9,10};
		ArrayList<Integer> actual=Q8EvenUsingStreams.evens(list);
		Object[] expected= {2,4,6,8,10};
		assertArrayEquals(expected,actual.toArray());
	}
	@Test
	void testNonEvenList() {
		int[] list= {1,2,3,4,5,6,7,8,9,10};
		ArrayList<Integer> actual=Q8EvenUsingStreams.evens(list);
		Object[] expected= {1,2,4,6,8,10};
		assertNotEquals(expected,actual.toArray());
	}

}
