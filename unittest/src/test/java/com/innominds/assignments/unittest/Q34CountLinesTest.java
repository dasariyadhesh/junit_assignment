package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Q34CountLinesTest {
	Q34CountLines q34Obj=new Q34CountLines();
	@Test
	void testFirstfile() {
		assertEquals(4,q34Obj.countLines("Q37Answers.txt"));
	}
	@Test
	void testnullfile() {
		assertEquals(0,q34Obj.countLines("empty.txt"));
	}
	@Test
	void testNotExistedFile() {
		assertEquals(0,q34Obj.countLines("Yadhesh.txt"));
	}

}
