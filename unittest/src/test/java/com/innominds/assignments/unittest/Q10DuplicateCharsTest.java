package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class Q10DuplicateCharsTest {
	Q10DuplicateChars q10Obj=new Q10DuplicateChars();
	@Test
	void testDuplicateChar() {
		char[] expected= {'a','b','c','d'};;
		assertEquals(Arrays.toString(expected),q10Obj.duplicateCharacters("aabbccdd").toString());
		
		
	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q10Obj.duplicateCharacters(null);} );
	}
	
}
