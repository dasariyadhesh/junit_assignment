package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Q11VehiclesTest {
	Q11Vehicles q11Obj=new Q11Vehicles();
	@Test
	void testCollision() {
		assertEquals("cars will Collide",q11Obj.avoidCollision("Left","Right"));
		
	}
	@Test
	void testNotCollision() {
		assertEquals("cars will not Collide",q11Obj.avoidCollision("Left","Left"));
		
	}

}
