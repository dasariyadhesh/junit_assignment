package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Q1VariableInstanceTest {
	@Test
	public void nonStaticInstanceTest() {
		Q1VariableInstance q1 = new Q1VariableInstance();
		Q1VariableInstance q2 = new Q1VariableInstance();
		Q1VariableInstance q3 = new Q1VariableInstance();
		System.out.println(q3.nonStaticInstance);
		assertEquals(1, q3.nonStaticInstance);
		
	}
	@Test
	public void staticInstaceTest() {
		Q1VariableInstance q4 = new Q1VariableInstance();
		Q1VariableInstance q5 = new Q1VariableInstance();
		Q1VariableInstance q6 = new Q1VariableInstance();
		System.out.println(q6.staticInstance);
		assertEquals(6,q6.staticInstance);
		
	}

}
