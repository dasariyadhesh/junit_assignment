package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Q18ClockTest {
	@Test
	void testValidTime() {
		String expected="Time is Valid";
		Q18Clock q18Obj=new Q18Clock(12,59,59);
		assertEquals(expected,q18Obj.isTimeValid());
		
	}
	@Test
	void testInValidTime() {
		String expected="Time is Invalid";
		Q18Clock q18Obj=new Q18Clock(33,59,59);
		assertEquals(expected,q18Obj.isTimeValid());
		
	}
	
	@Test 
	void testModeAm() {
		String expected="Time = 1:59:59 AM";
		Q18Clock q18Obj=new Q18Clock(01,59,59);
		assertEquals(expected,q18Obj.setTimeMode());
	}
	@Test 
	void testModePM() {
		String expected="Time = 1:59:59 PM";
		Q18Clock q18Obj=new Q18Clock(13,59,59);
		assertEquals(expected,q18Obj.setTimeMode());
	}


}
