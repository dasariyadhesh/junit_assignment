package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class Q33WordCountTest {
	Q33WordCount q33Obj=new Q33WordCount();
	@Test
	void testWordCount() {
		assertEquals(4,q33Obj.wordCount("Hi this is Yadehsh"));
	}
	@Test
	void testWordCountWithExtraSpace() {
		assertEquals(4,q33Obj.wordCount("Hi          this           is           Yadehsh"));
	}
	@Test
	void testEmptyString() {
		assertEquals(0,q33Obj.wordCount("           "));
	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q33Obj.wordCount(null);} );
	}
	

}
