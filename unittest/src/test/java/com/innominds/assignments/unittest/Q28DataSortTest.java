package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Q28DataSortTest {
	Q28DataSort q28Obj=new Q28DataSort();
	@Test
	void testStringChars() {
		char[] expected="123ADEHHSYadehhsy".toCharArray();
		assertArrayEquals(expected,q28Obj.dataSorting("321YADHESHyadhesh"));
	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q28Obj.dataSorting(null);} );
	}

}
