package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

class Q39CurrentDateTest {
	Q39CurrentDate Q39Obj=new Q39CurrentDate();
	@Test
	void testcurrentTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
		Date expected = new Date(System.currentTimeMillis());
		assertEquals(formatter.format(expected),Q39Obj.currentDate());
	}
	@Test
	void testnewTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
		Date currenttime = new Date(System.currentTimeMillis());
		Date expected =new Date(currenttime.getTime() + TimeUnit.HOURS.toMillis(10));
		assertEquals(formatter.format(expected),Q39Obj.add10HoursToDate());
	}

}
