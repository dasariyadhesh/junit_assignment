package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class Q7DateDiffTest {
	Q7DateDiff q7Obj=new Q7DateDiff(); 
	@Test
	void testDateDiff1() {
		assertEquals(0,q7Obj.dateDiff("19/05/2022","19/05/2022"));
	}
	@Test
	void testDateDiff2() {
		assertNotEquals(1,q7Obj.dateDiff("19/05/2000","19/05/2000"));
	}

}
