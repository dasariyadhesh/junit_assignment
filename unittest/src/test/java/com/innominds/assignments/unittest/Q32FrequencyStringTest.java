package com.innominds.assignments.unittest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.jupiter.api.Test;

class Q32FrequencyStringTest {
	Q32FrequencyString q32Obj = new Q32FrequencyString();
	@Test
	public void testAssertMap() {
		Map<Character, Integer> expected = new HashMap<>();
		expected.put(' ', 2);
		expected.put('p', 1);
		expected.put('a', 3);
		expected.put('t', 1);
		expected.put('e', 1);
		expected.put('v', 1);
		expected.put('h', 1);
		expected.put('j', 1);
		expected.put('l', 2);
		expected.put('o', 1);
		Map<Character, Integer> actual = q32Obj.stringFrequency("Hello Java PAT");
		// 1. Test equal, ignore order
		assertThat(expected, is(actual));
		// 2. Test size
		assertThat(actual.size(), is(10));
		// 3. Test expected entry, best!
		assertThat(actual, IsMapContaining.hasEntry('a', 3));
		// 4. Test expected key
		assertThat(actual, IsMapContaining.hasKey('h'));
		// 5. Test expected value
		assertThat(actual, IsMapContaining.hasValue(1));

	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q32Obj.stringFrequency(null);} );
	}
	
	

}
