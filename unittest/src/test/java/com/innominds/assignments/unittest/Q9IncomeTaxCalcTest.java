package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q9IncomeTaxCalcTest {
	Q9IncomeTaxCalc q9Obj=new Q9IncomeTaxCalc();
	@Test
	@DisplayName("income is 50k")
	void testIncometaxincome1() {
		assertEquals(0.0,q9Obj.incomeTaXCalc(50000));
	}
	
	@Test
	@DisplayName("income greater than 50k")
	void testIncometaxincome2() {
		assertEquals(1000,q9Obj.incomeTaXCalc(60000));
	}
	@Test
	@DisplayName("income greater than 60k")
	void testIncometaxincome3() {
		assertEquals(3000,q9Obj.incomeTaXCalc(70000));
	}
	@Test
	@DisplayName("income greater than 150k")
	void testIncometaxincome4() {
		assertEquals(34000,q9Obj.incomeTaXCalc(200000));
	}
	

}
