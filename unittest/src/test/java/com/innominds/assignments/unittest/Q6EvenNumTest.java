package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


class Q6EvenNumTest {

	Q6EvenNum q6Obj= new Q6EvenNum();

	@Test
	void testSaveEvenNumbers() {
		List<Integer> expectedList = Arrays.asList(2, 4, 6, 8, 10);
		assertArrayEquals(expectedList.toArray(), q6Obj.saveEvenNumbers(10).toArray());

	}

	@Test
	void testPrintEvenNumbers() {
		ArrayList<Integer> inputList = q6Obj.saveEvenNumbers(10);
		List<Integer> resultList = Arrays.asList(4, 8, 12, 16, 20);
		assertArrayEquals(resultList.toArray(), q6Obj.printEvenNumbers(inputList).toArray());
	}

}
