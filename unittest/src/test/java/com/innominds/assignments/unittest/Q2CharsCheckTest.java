package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
@TestInstance(Lifecycle.PER_CLASS)
class Q2CharsCheckTest {
	private Q2CharsCheck q2Obj;
	@BeforeAll
	 void createObject() {
		q2Obj = new Q2CharsCheck();
	}
	
	@Test
	void testUpperCase() {
		int aResult = q2Obj.checkUpper("Yadhesh");
		int eResult = 1;
		assertEquals(eResult , aResult);
	}
	@Test
	void testLoweCase() {
		int aResult = q2Obj.checkLower("Yadhesh");
		int eResult = 6;
		assertEquals(eResult , aResult);
	}
	@Test
	void testDigits() {
		int aResult = q2Obj.checkDigit("Yadhesh123");
		int eResult = 3;
		assertEquals(eResult , aResult);
	}
	@Test
	void testSplChars() {
		int aResult = q2Obj.checkOthers("Yadhesh");
		int eResult = 0;
		assertEquals(eResult , aResult);
	}
	@Test
	void testInvalidOutput() {
		assertThrows(NullPointerException.class,()->{q2Obj.checkLower(null);} );
		assertThrows(NullPointerException.class,()->{q2Obj.checkUpper(null);} );
		assertThrows(NullPointerException.class,()->{q2Obj.checkDigit(null);} );
		assertThrows(NullPointerException.class,()->{q2Obj.checkOthers(null);} );
	}

}
