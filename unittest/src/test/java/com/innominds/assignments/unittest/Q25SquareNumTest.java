package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class Q25SquareNumTest {
	Q25SquareNum q25Obj=new Q25SquareNum();
	@Test
	void testSquare() throws Exception {
		int[] expected= {1,4,9,16,25,36,49,64,81,100};
		assertArrayEquals(expected,q25Obj.squreNum(4));
	}

}
