package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Q22AddOpTest {

	Q22AddOp q22Obj = new Q22AddOp();
	@Test
	@DisplayName("Addition using method")
	void testAdd() {
		assertEquals(14,q22Obj.add(8, 6));
	}
	@Test
	@DisplayName("Addition using Lamda expression")
	void testAddtionUsingLambda() {
		assertEquals(10, q22Obj.additionOp.operation(1, 9));
	}
	

}
