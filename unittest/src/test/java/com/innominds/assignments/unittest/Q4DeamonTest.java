package com.innominds.assignments.unittest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class Q4DeamonTest {

		Q4Daemon thread = new Q4Daemon("T1");
		Daemon obj = new Daemon();
		@Test
		void testIsDaemonThread() {
			thread.setDaemon(true);
			assertTrue(obj.checkDaemonThread(thread));
		}
		@Test
		void testDaemonThreadException() {
			assertThrows(IllegalThreadStateException.class , ()->obj.daemonThreadException(thread));
		}
	}


